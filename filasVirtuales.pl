filasVirtuales(L):-
	L = [FilasVirtuales, Filas, Presentacion, Seguridad, EnlacePlataformaEstablecimiento, Responsable, Turnos, Cajas, Clasificacion, InterfazWeb, AppMovil, AutenticacionEstablecimiento, AutenticacionCliente, InformacionCLiente, TomarTurno, CancelarTurno, Alertas, FlujoTrabjo, Reportes, PorServicioProducto, Prioridad, GestionFuncionarios, GestionCajas, MetricasEstadisticas, AsignacionTurnos, Android, IOS, Windows10, LDAP, Formulario, Google, Facebook, Twitter, Outlook, PorElCliente, PorElEstablecimiento, AlCliente, AlFuncionario, TiempoEspera, TurnosPendientes, Atencion, Supervisores, Usabilidad, Dashboard, IntegracionGPS, Fisico, Web, Telefonico],
	fd_domain(L, 0, 1),
	% **** MANDATORIES ****
	FilasVirtuales #= 1,
	FilasVirtuales #= Filas,
	FilasVirtuales #= Presentacion,
	FilasVirtuales #= Seguridad,
	Filas #= Responsable,
	Filas #= Turnos,
	Filas #= Cajas,
	Presentacion #= InterfazWeb,
	Seguridad #= AutenticacionEstablecimiento,
	Turnos #= TomarTurno,
	Turnos #= CancelarTurno,
	Turnos #= Alertas,
	Turnos #= FlujoTrabjo,
	Turnos #= Reportes,
	InterfazWeb #= GestionFuncionarios,
	InterfazWeb #= GestionCajas,
	InterfazWeb #= MetricasEstadisticas,
	Alertas #= AlFuncionario,
	Reportes #= TiempoEspera,
	Reportes #= TurnosPendientes,
	GestionFuncionarios #= Atencion,
	MetricasEstadisticas #= Usabilidad,

	% **** OPTIONALS ****
	FilasVirtuales #>= EnlacePlataformaEstablecimiento,

	Filas #>= Clasificacion,
	Presentacion #>= AppMovil,
	Seguridad #>= AutenticacionCliente,
	EnlacePlataformaEstablecimiento #>= InformacionCLiente,

	InterfazWeb #>= AsignacionTurnos,

	Alertas #>= AlCliente,
	GestionFuncionarios #>= Supervisores,
	MetricasEstadisticas #>= Dashboard,
	
	PorElCliente #>= IntegracionGPS,

	% **** ALTERNATIVE ****
	Clasificacion #>= PorServicioProducto,
	Clasificacion #>= Prioridad,
	Clasificacion #=< PorServicioProducto + Prioridad,
	PorServicioProducto + Prioridad #=< Clasificacion * 2,

	AppMovil #>= Android,
	AppMovil #>= IOS,
	AppMovil #>= Windows10,
	AppMovil #=< Android + IOS + Windows10,
	Android + IOS + Windows10 #=< AppMovil *3,
	AutenticacionCliente #>= Google,
	AutenticacionCliente #>= Facebook,
	AutenticacionCliente #>= Twitter,
	AutenticacionCliente #>= Outlook,
	AutenticacionCliente #=< Google + Facebook + Twitter + Outlook,
	Google + Facebook + Twitter + Outlook #=< AutenticacionCliente,

	TomarTurno #>= PorElCliente,
	TomarTurno #>= PorElEstablecimiento,
	TomarTurno #=< PorElCliente + PorElEstablecimiento,
	PorElCliente + PorElEstablecimiento #=< TomarTurno,
	PorElEstablecimiento #>= Fisico,
	PorElEstablecimiento #>= Web,
	PorElEstablecimiento #>= Telefonico,
	PorElEstablecimiento #=< Fisico + Web + Telefonico,
	Fisico + Web + Telefonico #=< PorElEstablecimiento,

	% **** OR (Exclusion) ****
	AutenticacionEstablecimiento #>= LDAP,
	AutenticacionEstablecimiento #>= Formulario,
	LDAP ## Formulario #>= 1 * AutenticacionEstablecimiento,

	% **** IMPLICATIONS ****
	PorElCliente #==> AutenticacionCliente,
	PorElCliente #==> AppMovil,
	PorElCliente #==> AlCliente,
	PorElEstablecimiento #==> AsignacionTurnos,
	InformacionCLiente #==> AppMovil,

	fd_labeling(L).

	% **** VALIDACIONES *****
	% 1. modelo no vacio => ok
	% 2. modelo no puede ser falso (solo un producto )
	% 3. falsos opcionales (falsos que terminan siendo verdaderos)
	% 4. no debe tener elementos muertos
	% 5. no debe tener redundancias 

	% findall(L, filasVirtuales(L), Res).


